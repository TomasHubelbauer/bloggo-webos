# LG WebOS

## Virtualizing

- Install the latest version of VirtualBox
- Download the [Windows 10 ISO creation tool](https://www.microsoft.com/en-us/software-download/windows10)
- Run the Windows 10 ISO creation tool and configure it to make an ISO in English with Windows 10 Pro 64bit
- Create a Windows 10 Pro in VirtualBox and mount the generated Windows 10 Pro ISO file using Settings > Storage > CD
- Boot the VM and install Windows 10 Pro (unmount the ISO using Devices > Optical Drives > uncheck the ISO)
- Install VirtualBox Guest Additions by going to *Devices* > *Insert Guess Additions CD image…* in the VM window
- Turn on file sharing by going to *Machine* > *Settings* > *Shared Folders*, configure and then go to *Network* (`\\VBOXSVR\`)
- Turn on bidirectional clipboard and drag and drop by going to *Machine* > *General* > *Advanced* and enabling both
- Do not install [prerequisites for the WebOS SDK](http://webostv.developer.lge.com/sdk/download/download-sdk/?wos_flag=systemrequirements#systemrequirements) by hand:
  - [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (the provided link is not correct, the installer opens the correct one in a tab)
  - [VirtualBox](https://www.virtualbox.org/wiki/Download_Old_Builds_4_2) (the installer can install VirtualBox on its own)
- Download the [WebOS SDK](http://webostv.developer.lge.com/sdk/download/download-sdk/)
- Run the WebOS SDK installer and get prompted to install JRE, accept and see the opened tab, start the installation
- Rerun the installer and accept, get prompted to install VirtualBox and finish the VirtualBox installation
- Continue with the WebOS SDK installation using the network installer option
- [ ] Finalize
- Install LG Developer Mode application
- Use NPM package `ares-webos-sdk`
- `ares-device-setup` (use LG WebOS IDE if this is giving you trouble)
- `ares-generate` - in `index.html` add a redirect to a hosted app
- `ares-package`
- `ares-install`

## Containerizing

- [ ] Check out [this attempt to run the applications themselves in Docker](https://github.com/ericblade/webos-desktop-docker)
- [ ] Check out [this Docker image for VirtualBox in Docker](https://hub.docker.com/r/jencryzthers/vboxinsidedocker/) as the WebOS SDK's VirtualBox prerequisite
- [ ] Check out for example [this Docker OpenJDK image](https://hub.docker.com/_/openjdk/) as the WebOS SDK's JRE prerequisite (JDK includes JRE)
- [ ] See if a Doker container could be made combining the two above (and exporting display using X server maybe) to be able to run the WebOS SDK in Docker
- [ ] Experiment with exposing the screen [using VNC](https://stackoverflow.com/a/16311264/2715716) or [other ways](http://wiki.ros.org/docker/Tutorials/GUI)

## Contributing

- [ ] Action the [VirtualBox within VirtualBox tips](https://www.whonix.org/wiki/Nested_Virtualization#VirtualBox_inside_VirtualBox) if needed for emulator in the VM
- [ ] Explore the [LG WebOS resources](https://github.com/vitalets/awesome-smart-tv#lg-webos) in the Awesome SmartTV list
- [ ] Explore the [OpenWebOS projects](https://github.com/openwebos), `build-webos` in particular
